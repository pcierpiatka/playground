package edu.gitlab.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ModelTest {

  @Test
  void testMe() {
    //given
    //when
    Model model = new Model("Abc");

    //then
    Assertions.assertEquals(model.name, "Abc");
  }
}