package edu.gitlab.model;

import java.util.concurrent.ThreadLocalRandom;

public class Model {

  public final int number;
  public final String name;

  public Model(String name) {
    this.name = name;
    this.number = ThreadLocalRandom.current().nextInt();
  }
}
